// Import Packages
const express = require('express');
const morgan = require('morgan');

// App
const app = express();

// Morgan
app.use(morgan('tiny'));

// First Route
app.get('/', (req, res) => {
    res.json({ message: 'Hello World' });
});

// enabled routing to posts.routes.js
app.use(morgan('tiny'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(require('./routes/index.routes'));

// Starting Server
app.listen('1337');
